FROM python:3.9-slim-buster

WORKDIR /usr/local/src

COPY . .

RUN pip install --no-cache-dir -r  requirements.txt

CMD while true; do sleep 1; done
