import pandas as pd
import numpy as np

from src.functions import add


def test_add():
    assert add(10,1) == 11

def test_data_fields():
    df_1 = pd.read_csv('cities.csv')
    fields = np.array(['LatD', ' LatM', ' LatS', ' NS', ' LonD', ' LonM', ' LonS', ' EW', ' City',
' State'])

    columns = df_1.columns.values

    assert np.array_equal(fields, columns)
